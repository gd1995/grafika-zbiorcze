﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        Point startXY;
        Point stopXY;
        ObservableCollection<Line> lines;
        ObservableCollection<Ellipse> ellipses;
        ObservableCollection<Rectangle> rectangles;


        Shape selected;

        public MainWindow()
        {
            InitializeComponent();

            lines = new ObservableCollection<Line>();
            ellipses = new ObservableCollection<Ellipse>();
            rectangles = new ObservableCollection<Rectangle>();
          
        }

        private void startPainting(object sender, MouseButtonEventArgs e)
        {
            startXY = e.GetPosition((Canvas)sender);
        }

        private void stopPainting_mlbu(object sender, MouseButtonEventArgs e)
        {
            Canvas drawplace = (Canvas)sender;

            stopXY = e.GetPosition(drawplace);

            if (click_rb.IsChecked == true)
            {
                selected = null;

                var mouseWasDownOn = e.Source as FrameworkElement;
                if (mouseWasDownOn != null)
                {
                    string elementName = mouseWasDownOn.Name;
                    foreach (Shape child in canvasExerciseOne_c.Children)
                    {
                        if (child.Name == elementName)
                        {
                            selected = child;
                            break;
                        }
                    }
                }


                shapeX1_tb.Text = "";
                shapeY1_tb.Text = "";
                shapeX2_tb.Text = "";
                shapeY2_tb.Text = "";
                shapeHeight_tb.Text = "";
                shapeWidth_tb.Text = "";

                if (selected == null)
                {

                }
                else if (selected.GetType().ToString() == "System.Windows.Shapes.Line")
                {
                    Line tmp = (Line)selected;

                    shapeX1_tb.Text = tmp.X1.ToString();
                    shapeY1_tb.Text = tmp.Y1.ToString();
                    shapeX2_tb.Text = tmp.X2.ToString();
                    shapeY2_tb.Text = tmp.Y2.ToString();

                }
                else if (selected.GetType().ToString() == "System.Windows.Shapes.Ellipse")
                {
                    Ellipse tmp = (Ellipse)selected;

                    shapeX1_tb.Text = tmp.Margin.Left.ToString();
                    shapeY1_tb.Text = tmp.Margin.Top.ToString();
                    shapeHeight_tb.Text = tmp.Height.ToString();
                    shapeWidth_tb.Text = tmp.Width.ToString();
                }
                else if (selected.GetType().ToString() == "System.Windows.Shapes.Rectangle")
                {
                    Rectangle tmp = (Rectangle)selected;

                    shapeX1_tb.Text = tmp.Margin.Left.ToString();
                    shapeY1_tb.Text = tmp.Margin.Top.ToString();
                    shapeHeight_tb.Text = tmp.Height.ToString();
                    shapeWidth_tb.Text = tmp.Width.ToString();





                }
            }
            else if (line_rb.IsChecked == true)
            {
                Line myLine = new Line();
                myLine.Name = "Line_" + lines.Count;
                myLine.Stroke = Brushes.Black;
                myLine.X1 = startXY.X;
                myLine.X2 = stopXY.X;
                myLine.Y1 = startXY.Y;
                myLine.Y2 = stopXY.Y;
                myLine.HorizontalAlignment = HorizontalAlignment.Left;
                myLine.VerticalAlignment = VerticalAlignment.Center;
                myLine.StrokeThickness = 2;
                canvasExerciseOne_c.Children.Add(myLine);
                lines.Add(myLine);
            }
            else if (circle_rb.IsChecked == true)
            {
                Point tmp = new Point();
                if (startXY.X >= stopXY.X)
                    tmp.X = startXY.X;
                else
                    tmp.X = stopXY.X;

                if (startXY.Y >= stopXY.Y)
                    tmp.Y = startXY.Y;
                else
                    tmp.Y = stopXY.Y;

                Ellipse myEllipse = CreateEllipse(Math.Abs(startXY.X - stopXY.X), Math.Abs(startXY.Y - stopXY.Y), tmp.X, tmp.Y);
                myEllipse.Name = "Ellipse_" + ellipses.Count;

                myEllipse.Stroke = Brushes.Black;

                myEllipse.HorizontalAlignment = HorizontalAlignment.Left;
                myEllipse.VerticalAlignment = VerticalAlignment.Center;
                myEllipse.StrokeThickness = 2;
                canvasExerciseOne_c.Children.Add(myEllipse);
                ellipses.Add(myEllipse);
            }
            else if (rectangle_rb.IsChecked == true)
            {
                Point tmp = new Point();
                if (startXY.X >= stopXY.X)
                    tmp.X = startXY.X;
                else
                    tmp.X = stopXY.X;

                if (startXY.Y >= stopXY.Y)
                    tmp.Y = startXY.Y;
                else
                    tmp.Y = stopXY.Y;

                Rectangle myRectangle = CreateRectangle(Math.Abs(startXY.X - stopXY.X), Math.Abs(startXY.Y - stopXY.Y), tmp.X, tmp.Y);
                myRectangle.Name = "Rectangle_" + rectangles.Count;
                myRectangle.Stroke = Brushes.Black;

                myRectangle.HorizontalAlignment = HorizontalAlignment.Left;
                myRectangle.VerticalAlignment = VerticalAlignment.Center;
                myRectangle.StrokeThickness = 2;
                canvasExerciseOne_c.Children.Add(myRectangle);
                rectangles.Add(myRectangle);
            }
        }

        Ellipse CreateEllipse(double width, double height, double desiredCenterX, double desiredCenterY)
        {
            Ellipse ellipse = new Ellipse { Width = width, Height = height };
            double left = desiredCenterX - width;
            double top = desiredCenterY - height;

            ellipse.Margin = new Thickness(left, top, 0, 0);
            return ellipse;
        }

        Ellipse CreateEllipse1(double width, double height, double margin1, double margin2)
        {
            Ellipse ellipse = new Ellipse { Width = width, Height = height };

            ellipse.Margin = new Thickness(margin1, margin2, 0, 0);
            return ellipse;
        }

        Rectangle CreateRectangle(double width, double height, double desiredCenterX, double desiredCenterY)
        {
            Rectangle rectangle = new Rectangle { Width = width, Height = height };
            double left = desiredCenterX - width;
            double top = desiredCenterY - height;

            rectangle.Margin = new Thickness(left, top, 0, 0);
            return rectangle;
        }

        Rectangle CreateRectangle1(double width, double height, double margin1, double margin2)
        {
            Rectangle rectangle = new Rectangle { Width = width, Height = height };

            rectangle.Margin = new Thickness(margin1, margin2, 0, 0);
            return rectangle;
        }





        private void ShapeSave_Click(object sender, RoutedEventArgs e)
        {
            if (selected == null)
            {

                if (line_rb.IsChecked == true)
                {
                    if (shapeX1_tb != null && shapeX2_tb != null && shapeY1_tb != null && shapeY2_tb != null)
                    {
                        Line myLine = new Line();
                        myLine.Name = "Line_" + lines.Count;
                        myLine.Stroke = Brushes.Black;
                        myLine.X1 = Double.Parse(shapeX1_tb.Text);
                        myLine.X2 = Double.Parse(shapeX2_tb.Text);
                        myLine.Y1 = Double.Parse(shapeY1_tb.Text);
                        myLine.Y2 = Double.Parse(shapeY2_tb.Text);
                        myLine.HorizontalAlignment = HorizontalAlignment.Left;
                        myLine.VerticalAlignment = VerticalAlignment.Center;
                        myLine.StrokeThickness = 2;
                        canvasExerciseOne_c.Children.Add(myLine);
                        lines.Add(myLine);
                    }
                }
                else if (circle_rb.IsChecked == true)
                {
                    if (shapeWidth_tb != null && shapeX1_tb != null && shapeY1_tb != null && shapeHeight_tb != null)
                    {
                        Ellipse myEllipse = CreateEllipse1(Double.Parse(shapeWidth_tb.Text), Double.Parse(shapeHeight_tb.Text), Double.Parse(shapeX1_tb.Text), Double.Parse(shapeY1_tb.Text));
                        myEllipse.Name = "Ellipse_" + ellipses.Count;
                        myEllipse.Stroke = Brushes.Black;

                        myEllipse.HorizontalAlignment = HorizontalAlignment.Left;
                        myEllipse.VerticalAlignment = VerticalAlignment.Center;
                        myEllipse.StrokeThickness = 2;
                        canvasExerciseOne_c.Children.Add(myEllipse);
                        ellipses.Add(myEllipse);
                    }
                }


                else if (rectangle_rb.IsChecked == true)
                {
                    if (shapeWidth_tb != null && shapeX1_tb != null && shapeY1_tb != null && shapeHeight_tb != null)
                    {
                        Rectangle myRectangle = CreateRectangle1(Double.Parse(shapeWidth_tb.Text), Double.Parse(shapeHeight_tb.Text), Double.Parse(shapeX1_tb.Text), Double.Parse(shapeY1_tb.Text));
                        myRectangle.Name = "Rectangle_" + rectangles.Count;
                        myRectangle.Stroke = Brushes.Black;
                        myRectangle.HorizontalAlignment = HorizontalAlignment.Left;
                        myRectangle.VerticalAlignment = VerticalAlignment.Center;
                        myRectangle.StrokeThickness = 2;
                        canvasExerciseOne_c.Children.Add(myRectangle);
                        rectangles.Add(myRectangle);
                    }
                }



            }
            else if (selected.GetType().ToString() == "System.Windows.Shapes.Line")
            {
                Line tmp = (Line)selected;
                if (shapeX1_tb.Text != null)
                    tmp.X1 = Double.Parse(shapeX1_tb.Text);
                if (shapeX1_tb.Text != null)
                    tmp.Y1 = Double.Parse(shapeY1_tb.Text);
                if (shapeX1_tb.Text != null)
                    tmp.X2 = Double.Parse(shapeX2_tb.Text);
                if (shapeX1_tb.Text != null)
                    tmp.Y2 = Double.Parse(shapeY2_tb.Text);


            }
            else if (selected.GetType().ToString() == "System.Windows.Shapes.Ellipse")
            {
                Ellipse tmp = (Ellipse)selected;


                if (shapeX1_tb.Text != null && shapeX1_tb.Text != null)
                    tmp.Margin = new Thickness(Double.Parse(shapeX1_tb.Text), Double.Parse(shapeY1_tb.Text), 0, 0);
                if (shapeX1_tb.Text != null)
                    tmp.Height = Double.Parse(shapeHeight_tb.Text);
                if (shapeX1_tb.Text != null)
                    tmp.Width = Double.Parse(shapeWidth_tb.Text);

            }
            else if (selected.GetType().ToString() == "System.Windows.Shapes.Rectangle")
            {
                Rectangle tmp = (Rectangle)selected;

                if (shapeX1_tb.Text != null && shapeX1_tb.Text != null)
                    tmp.Margin = new Thickness(Double.Parse(shapeX1_tb.Text), Double.Parse(shapeY1_tb.Text), 0, 0);
                if (shapeX1_tb.Text != null)
                    tmp.Height = Double.Parse(shapeHeight_tb.Text);
                if (shapeX1_tb.Text != null)
                    tmp.Width = Double.Parse(shapeWidth_tb.Text);


            }
        }

    }
}


