﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace WpfApplication2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            RenderOptions.SetBitmapScalingMode(image_i, BitmapScalingMode.NearestNeighbor);
            quality_s.Value = 85;
        }

        private void loadImage(object sender, RoutedEventArgs e)
        {
            try
            {
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

                dlg.Filter = "PPM Files (*.ppm)|*.ppm|JPEG Files (*.jpg;*.jpeg)|*.jpg;*.jpeg|PNG Files (*.png)|*.png";

                Nullable<bool> result = dlg.ShowDialog();

                if (result == true)
                {
                    string filename = dlg.FileName;

                    String[] substrings = filename.Split('.');

                    if (substrings[substrings.Length - 1] == "jpg" ||  substrings[substrings.Length - 1] == "jpeg" || substrings[substrings.Length - 1] == "png")
                    {
                        BitmapImage bitimg = new BitmapImage();
                        bitimg.BeginInit();
                        bitimg.UriSource = new Uri(filename);
                        bitimg.EndInit();

                        image_i.Source = bitimg;
                    }
                    else if (substrings[substrings.Length - 1] == "ppm")
                    {
                        StreamReader sr = new StreamReader(filename);

                        String version = "";
                        int startPosition = 0;
                        int rows = 0;
                        int columns = 0;
                        int max = 0;

                        byte[] buffer = new byte[1];
                        int color = 0;
                        String line;
                        String[] data;
                        int counter = 0;
                        List<byte> pom = new List<byte>();
                        bool escape = false;

                        while ((line = sr.ReadLine()) != null)
                        {
                            startPosition += (line.Length + 1);
                            if (line == "" || line[0] == '#')
                                continue;
                            line = Regex.Replace(line, @"\s+", " ");
                            data = line.Split(' ');
                            foreach (var i in data)
                            {
                                if (i == "")
                                    continue;
                                if (i[0] == '#')
                                    break;

                                if (counter > 3)
                                {
                                    color = Int32.Parse(i);
                                    if (max != 255)
                                        color = color * 255 / max;
                                    pom.Add((byte)color);
                                }
                                else if (counter == 0)
                                    version = i;
                                else if (counter == 1)
                                    rows = Int32.Parse(i);
                                else if (counter == 2)
                                    columns = Int32.Parse(i);
                                else if (counter == 3)
                                {
                                    max = Int32.Parse(i);
                                    if (version == "P6")
                                        escape = true;
                                }
                                 counter++;

                            }
                            if (escape == true)
                                break;
                        }
                        sr.Close();

                        if (version == "P6")
                        {
                            BinaryReader br = new BinaryReader(new FileStream(dlg.FileName, FileMode.Open));
                            br.BaseStream.Position = startPosition;
                            buffer = br.ReadBytes(rows * columns * 3);
                            br.Close();
                        }
                        if (version != "P6")
                            buffer = pom.ToArray();

                        var pixelFormat = PixelFormats.Rgb24;
           
                          var step = 3 * rows;
                        

                        var bitmap = BitmapSource.Create(rows, columns, 0d, 0d, pixelFormat, null, buffer, step);

                        image_i.Source = bitmap;
                    }
                }
            }
            catch (ArgumentException)
            {
                MessageBox.Show("ERROR! Bład pliku.");
            }
        }

        private void saveImage(object sender, RoutedEventArgs e)
        {
            try
            {
                Microsoft.Win32.SaveFileDialog saveDialog = new Microsoft.Win32.SaveFileDialog();

                saveDialog.FileName = "Bez nazwy";
                saveDialog.DefaultExt = "jpg";
                saveDialog.Filter = "JPG images (*.jpg)|*.jpg";

                Nullable<bool> result = saveDialog.ShowDialog();

                if (result == true)
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    Guid photoID = System.Guid.NewGuid();
                    String photolocation = saveDialog.FileName;

                    encoder.Frames.Add(BitmapFrame.Create((BitmapSource)image_i.Source));
                    encoder.QualityLevel = (int)quality_s.Value;

                    using (var filestream = new FileStream(photolocation, FileMode.Create))
                        encoder.Save(filestream);
                }
            }
            catch (ArgumentException)
            {
                MessageBox.Show("ERROR! Błąd zapisu lub brak pliku");
            }
        }
    }
}
