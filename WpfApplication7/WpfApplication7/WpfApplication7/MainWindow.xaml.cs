﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication7
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private Polygon polygon;
        private Ellipse landmark;
        private Point? pom;

        public MainWindow()
        {
            InitializeComponent();
            polygon = new Polygon();
            polygon.Fill = Brushes.GreenYellow;
            polygon.Stroke = Brushes.Black;
            vectorCanvas_c.Children.Add(polygon);

            landmark = new Ellipse();
            landmark.Height = 5;
            landmark.Width = 5;
            landmark.Margin = new Thickness(-10, -10, 0, 0);
            landmark.Fill = new SolidColorBrush(Colors.Red);
            vectorCanvas_c.Children.Add(landmark);
        }

        private void Load(object sender, RoutedEventArgs e)
        {
            removePoints_Click(sender, e);
            try
            {
                using (StreamReader sr = new StreamReader("listapunktow.txt"))
                {
                    string line;
                    line = sr.ReadLine();
                    string[] points = line.Split(null);

                    landmarkX_tb.Text = points[0];
                    landmarkY_tb.Text = points[1];
                    landmark.Margin = new Thickness(Double.Parse(points[0]), Double.Parse(points[1]), 0, 0);
                    enterLandmark_rb.IsChecked = true;

                    while (sr.Peek() >= 0)
                    {
                        line = sr.ReadLine();
                        points = line.Split(null);
                        polygon.Points.Add(new Point(Double.Parse(points[0]), Double.Parse(points[1])));
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Błąd odczytu");
            }
        }

        private void Save(object sender, RoutedEventArgs e)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter("listapunktow.txt"))
                {
                    if (landmarkX_tb.Text.ToString() != "")
                    {
                        sw.WriteLine(landmarkX_tb.Text.ToString() + " " + landmarkY_tb.Text.ToString());
                    }
                    else
                    {
                        sw.WriteLine("0 0");
                    }
                    foreach (Point point in polygon.Points)
                    {
                        sw.WriteLine(point.X.ToString() + " " + point.Y.ToString());
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Błąd zapisu");
            }
        }

        private void vectorCanvas_c_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (createTool_rb.IsChecked == true)
            {
                Point tmp = e.GetPosition(vectorCanvas_c);
                polygon.Points.Add(tmp);
            }
            else if (moveTool_rb.IsChecked == true)
            {
                pom = new Point(e.GetPosition(polygon).X, e.GetPosition(polygon).Y);
            }
            else if (rotateTool_rb.IsChecked == true || resizeTool_rb.IsChecked == true)
            {
                pom = new Point(e.GetPosition(polygon).X, e.GetPosition(polygon).Y);
                if (pointLandmark_rb.IsChecked == true)
                {
                    double x = e.GetPosition(vectorCanvas_c).X;
                    double y = e.GetPosition(vectorCanvas_c).Y;

                    landmarkX_tb.Text = x.ToString();
                    landmarkY_tb.Text = y.ToString();

                    landmark.Margin = new Thickness(x, y, 0, 0);

                    enterLandmark_rb.IsChecked = true;
                    vectorCanvas_c.Cursor = Cursors.SizeNESW;
                }
            }
        
        }

        private void vectorCanvas_c_MouseMove(object sender, MouseEventArgs e)
        {
            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                if (moveTool_rb.IsChecked == true)
                {
                    Point difference = new Point(e.GetPosition(vectorCanvas_c).X - pom.Value.X, e.GetPosition(vectorCanvas_c).Y - pom.Value.Y);
                    for (int i = 0; i < polygon.Points.Count; i++)
                    {
                        polygon.Points[i] = new Point(polygon.Points[i].X + difference.X, polygon.Points[i].Y + difference.Y);
                    }
                    pom = new Point(e.GetPosition(polygon).X, e.GetPosition(polygon).Y);
                }
                else if (rotateTool_rb.IsChecked == true)
                {
                    Point difference = new Point(e.GetPosition(vectorCanvas_c).X - pom.Value.X, e.GetPosition(vectorCanvas_c).Y - pom.Value.Y);

                    double landmarkX = Double.Parse(landmarkX_tb.Text);
                    double landmarkY = Double.Parse(landmarkY_tb.Text);

                    int dx = (int)Math.Abs(difference.X);
                    int dy = (int)Math.Abs(difference.Y);
                    double angleRadians = Math.Atan2(dy, dx);
                    double angleDegrees = ((angleRadians * 180) / Math.PI / 1000);

                    if (difference.X + difference.Y < 0)
                        angleDegrees *= -1;
                    for (int i = 0; i < polygon.Points.Count; i++)
                    {
                        polygon.Points[i] = new Point(landmarkX + (polygon.Points[i].X - landmarkX) * Math.Cos(angleDegrees) - (polygon.Points[i].Y - landmarkY) * Math.Sin(angleDegrees),
                            landmarkY + (polygon.Points[i].X - landmarkX) * Math.Sin(angleDegrees) + (polygon.Points[i].Y - landmarkY) * Math.Cos(angleDegrees));
                    }
                    pom = new Point(e.GetPosition(polygon).X, e.GetPosition(polygon).Y);
                }
                else if (resizeTool_rb.IsChecked == true)
                {
                    Point difference = new Point(e.GetPosition(vectorCanvas_c).X - pom.Value.X, e.GetPosition(vectorCanvas_c).Y - pom.Value.Y);

                    double landmarkX = Double.Parse(landmarkX_tb.Text);
                    double landmarkY = Double.Parse(landmarkY_tb.Text);

                    double ratioK = 1.01;
                    if (difference.X + difference.Y < 0)
                        ratioK = 0.99;

                    for (int i = 0; i < polygon.Points.Count; i++)
                    {
                        polygon.Points[i] = new Point(polygon.Points[i].X * ratioK + (1 - ratioK) * landmarkX, polygon.Points[i].Y * ratioK + (1 - ratioK) * landmarkY);
                    }
                    pom = new Point(e.GetPosition(polygon).X, e.GetPosition(polygon).Y);
                }
            }
         
        }

        private void vectorCanvas_c_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            double x = e.GetPosition(vectorCanvas_c).X;
            double y = e.GetPosition(vectorCanvas_c).Y;

            landmarkX_tb.Text = x.ToString();
            landmarkY_tb.Text = y.ToString();

            landmark.Margin = new Thickness(x, y, 0, 0);
        }

        private void landmark_tb_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                double x = Double.Parse(landmarkX_tb.Text);
                double y = Double.Parse(landmarkY_tb.Text);

                landmark.Margin = new Thickness(x, y, 0, 0);

            }
            catch (FormatException)
            {
                MessageBox.Show("Utwórz punkt orientacyjny");
            }
        }

        private void pointLandmark_rb_Click(object sender, RoutedEventArgs e)
        {
            vectorCanvas_c.Cursor = Cursors.Cross;
        }

        private void addPoint_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                double x = Double.Parse(newPointX.Text);
                double y = Double.Parse(newPointY.Text);
                Point tmp = new Point(x, y);
                polygon.Points.Add(tmp);
            }
            catch (FormatException)
            {
                MessageBox.Show("Wprowadź koordynaty");
            }
            
        }
        private void removePoints_Click(object sender, RoutedEventArgs e)
        {
            polygon.Points.Clear();
            landmark.Margin = new Thickness(-10, -10, 0, 0);
            landmarkX_tb.Text = "";
            landmarkY_tb.Text = "";
            pointLandmark_rb.IsChecked = true;
       
        }

        private void move_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                double x = Double.Parse(moveVectorX.Text);
                double y = Double.Parse(moveVectorY.Text);
                for (int i = 0; i < polygon.Points.Count; i++)
                {
                    polygon.Points[i] = new Point(polygon.Points[i].X + x, polygon.Points[i].Y + y);
                }
            }
            catch (FormatException)
            {
                MessageBox.Show("Wprowadź wektor");
            }
         
        }

        private void vektorToolChange_Click(object sender, RoutedEventArgs e)
        {
            if (createTool_rb.IsChecked == true)
            {
                vectorCanvas_c.Cursor = Cursors.Cross;
            }
            else if (moveTool_rb.IsChecked == true)
            {
                vectorCanvas_c.Cursor = Cursors.SizeAll;
            }
            else if (rotateTool_rb.IsChecked == true)
            {
                if (pointLandmark_rb.IsChecked == true)
                    vectorCanvas_c.Cursor = Cursors.Cross;
                else
                    vectorCanvas_c.Cursor = Cursors.SizeNESW;
            }
            else if (resizeTool_rb.IsChecked == true)
            {
                if (pointLandmark_rb.IsChecked == true)
                    vectorCanvas_c.Cursor = Cursors.Cross;
                else
                    vectorCanvas_c.Cursor = Cursors.SizeNWSE;
            }
           
        }

        private void rotate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                double landmarkX = Double.Parse(landmarkX_tb.Text);
                double landmarkY = Double.Parse(landmarkY_tb.Text);
                double angleDegrees = Double.Parse(angle_tb.Text);

                for (int i = 0; i < polygon.Points.Count; i++)
                {
                    polygon.Points[i] = new Point(landmarkX + (polygon.Points[i].X - landmarkX) * Math.Cos(angleDegrees) - (polygon.Points[i].Y - landmarkY) * Math.Sin(angleDegrees),
                                                    landmarkY + (polygon.Points[i].X - landmarkX) * Math.Sin(angleDegrees) + (polygon.Points[i].Y - landmarkY) * Math.Cos(angleDegrees));
                }
           
            }
            catch (FormatException)
            {
                MessageBox.Show("Brak kąta obrotu lub punktu orientacyjnego");
            }
        }
        private void resize_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                double landmarkX = Double.Parse(landmarkX_tb.Text);
                double landmarkY = Double.Parse(landmarkY_tb.Text);
                double ratioK = Double.Parse(ratioK_tb.Text);

                for (int i = 0; i < polygon.Points.Count; i++)
                {
                    polygon.Points[i] = new Point(polygon.Points[i].X * ratioK + (1 - ratioK) * landmarkX, polygon.Points[i].Y * ratioK + (1 - ratioK) * landmarkY);
                }
              
            }
            catch (FormatException)
            {
                MessageBox.Show("Brak współczynnika skalowania lub punktu orientacyjnego");
            }
        }




    }
}
